#include <stdio.h>
#include <stdlib.h>

/* Función punto de entrada */
int  main(){

    printf("%c", 97);
    printf("%c", 0x61);
    printf("%c", 'a');

    puts("");
    printf("%i", 97);
    /*
     *     4 => '4'
     *         4 + '0'
     *             4 + 0x30
     *                 */
    /*
     *     scanf("%i)
     *
     *         '4'
     *             '4'-'0' => 4
     *                 '7'
     *                     4*10 => 40
     *                         '7' - '0' => 7
     *                             40 + 7 = 47
     *                                 '5'
     *                                     47 * 10 => 470
     *                                         '5' - '0' => 5
     *                                             470 + 5
     *                                                 475
     *                                                 */
    puts("");

    printf("%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n%2i\n", 1,2,3,4,5,6,7,8,9,10,11,12,13,14);

    printf("%lf\n", 3.2);
    printf("%.2lf\n", 3.2);
    printf("%6.2lf\n", 3.2);

    printf("\thola\n");

    int numero;
    printf("Num: ");
    scanf(" %i", &numero);
    printf("Numero => [%p]: %i\n",&numero,  numero);
    printf("Linea %i\n", 47);


    int dia, annio;
    printf("Nacimiento dd/mm/aaaa: ");
    scanf(" %i/%*i/%i", &dia, &annio); // Carácter de supresión de asignación

    char hex[32];
    scanf(" %[0-9abcdefA-F]", hex);   /* Conjunto de selección */
    scanf(" %[^0-9abcdefA-F]", hex);  /* Conjunto de selección inverso */

    return EXIT_SUCCESS;
}

