#include<stdlib.h>
#include<stdio.h>

/* Función punto de entrada */
int main(){

	int dn, mn, an; //dia, mes y año de nacimiento
	int dh, mh, ah; //d, m, a de hoy
	int dias;

	printf("Nacimiento: ");
	scanf(" %d/%d/%i", &dn, &mn, &an);
	printf("Hoy: ");
	scanf(" %d/%d/%i", &dh, &mh, &ah);

	dias = dh - dn + (mh - mn) * 30 + (ah - an) * 365;
       	printf("Has vivivdo %i dias.\n", dias);

	return EXIT_SUCCESS; 
}
