#include <stdio.h>

int main(){

	char nombre[100];
	int edad, dias;

	printf("\n\nIntroduce tu nombre y pulsa INTRO: ");
	scanf("%s", nombre);

	printf("\n\n\Introduce tu edad y pulsa INTRO: ");
	scanf("%d", &edad);

	dias = edad * 365;  

	printf("\n\n\n\%s, has vivido %d días", nombre, dias);
	printf("\n\n\n\nPulsa cualquier tecla para salir.\n");
}

