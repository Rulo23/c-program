#include<stdio.h>
#include<stdlib.h>


#define MAX 0x100

/*Parámetro formal(Definición de la función)*/
void pon_titulo(int tabla){
	char titulo[MAX]
	sprintf(titulo, "toilet -fpagga --metal Tabla del %i", tabla);
	system(titulo);
}	
/*Función de entrada*/
int main(){

	/*Declaración de variables*/
	int tabla, op1 = 1;

	/*Menú*/
	printf("¿Qué tabla quieres?\n");
	printf("Tabla: ");
	scanf(" %i", &tabla);

	/*Llamada*/	
	pon_titulo(tabla); /*Llamada con parámetro actual 5*/

	/*Resultados*/
	printf("%ix%i=%i\n",op1, tabla, op1 * tabla);
	op1++;
	printf("%ix%i=%i\n",op1, tabla, op1 * tabla);

	return EXIT_SUCCESS;
}
