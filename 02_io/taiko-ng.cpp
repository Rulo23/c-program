

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define VECES 1000
#define BTU   100000

int main(){

	for (int i=0; i<VECES; i++) {
	fputc('\a', stderr);
	usleep(BTU);
	}


	return EXIT_SUCCESS;
}
