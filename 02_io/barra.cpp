#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define N     100
#define DELAY 100000

/* Función punto de entrada */
int  main(){

    for (int veces=0; veces<N; veces++){
            for (int columna=0; columna<veces; columna++)
                fprintf(stderr, "=");
            fprintf(stderr, "> %i%%\r", veces);
            usleep(DELAY);
        }
    printf("\n");

    return EXIT_SUCCESS;
}

