#include <stdio.h>
#include <stdlib.h>

#define ROJO 4
#define AMAR 2
#define AZUL 1

/* Función punto de entrada */
int  main(){

    char respuesta;
    int color = 0;

    /* Bloque de entrada de datos */
    printf("Ves rojo? (s/N): ");
    scanf(" %c", &respuesta);
    if (respuesta == 's')
        color |= ROJO;

    printf("Ves amarillo? (s/n): ");
    scanf(" %c", &respuesta);
    if (respuesta == 's')
        color |= AMAR;

    printf("Ves azul? (s/n): ");
    scanf(" %c", &respuesta);
    if (respuesta == 's')
        color |= AZUL;

    switch(color) {
        case 0:
            printf("Negro.\n");
            break;
        case 1:
            printf("Azul.\n");
            break;
        case 2:
            printf("Amrillo.\n");
            break;
        case 3:
            printf("Verde.\n");
            break;
        case 4:
            printf("Rojo.\n");
            break;
        case 5:
            printf("Morado.\n");
            break;
        case 6:
            printf("Naranja.\n");
            break;
        case 7:
            printf("Blanco.\n");
            break;


    }

    return EXIT_SUCCESS;
}
